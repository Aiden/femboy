#!/usr/bin/python3
# This is a story

from os import system, name
import time

def clear():
    if name == 'nt':
        _ = system('cls')
    else:
        _ = system('clear')

def invalid():
    print('Invalid response. Type capital "Y" or "N".')

def story():
    while True:
        r = input()
        if r== 'N':
            clear()
            print('\nYou decide to walk around. The streets are busy as usual and there seems to be constant traffic. A line of cars are parked in front of your apartment. You find your keys attached to your belt loop but find no car keys.\nSuddenly, in your fuzzy daze, you get ran over by a speeding city bus. You are now dead.\nType "restart" to start over.')
            while True:
                r2 = input()
                if r2 == 'restart':
                    clear()
                    begin()
                else:
                    print('\nYou are dead. Type "restart" to start over.')
        elif r == 'Y':
            clear()
            print('\nYou walk forward to the first door. From your stride, you assume that you\'re in your late teens or early twenties. You arrive at the door, ignoring the honking taxis, and open it.\nAfter walking through the main door, you wonder who exactly you are. Do you reach into your pockets to find out?')
            global wallet_check
            while True:
                r3 = input()
                if r3 == 'Y':
                    clear()
                    wallet_check = True
                    wallet_decide()
                elif r3 == 'N':
                    clear()
                    wallet_check = False
                    wallet_decide()
                else:
                    print('Invalid response. Type capital "Y" or "N".')
        else:
            print('Invalid response. Type capital "Y" or "N".')

def wallet_decide():
    global comp_check
    if wallet_check == True:
        print("\nYou pull out your phone, but it's locked. Putting that away, you take out your wallet and find a New York driver's license. Your name is apparently Aiden Hendrix, born August 20, 2001. You are eighteen years old, five foot eight, and ninety pounds.")
    elif wallet_check == False:
        print('\nYou decide not to check your pockets. Maybe you don\'t even want to know anyway.')
    while True:
        print('Do you go upstairs?')
        r = input()
        if r == 'Y':
            clear()
            print('\nYou walk up the wooden staircase until you reach the second floor. Your keys suggest you live in room 216. About halfway down the hallway, you find room 216. You unlock the door and enter your room, closing the door behind you.\nInside, there is a bed, a laptop, anime posters on the wall, and a few other items.')
            print('Do you check your computer?')
            while True:
                r2 = input()
                if r2 == 'Y':
                    clear()
                    comp_check = True
                    room()
                elif r2 == 'N':
                    clear()
                    comp_check = False
                    room()
                else:
                    print('Invalid response. Type capital "Y" or "N".')
        elif r == 'N':
            while True:
                clear()
                print('\nYou feel that your room is upstairs. There is nothing down here.')
                print('Do you go upstairs?')
                r2 = input()
                if r2 == 'Y':
                    clear()
                    print('\nYou walk up the wooden staircase until you reach the second floor. Your keys suggest you live in room 216. About halfway down the hallway, you find room 216. You unlock the door and enter your room, closing the door behind you.\nInside, there is a bed, a laptop, anime posters on the wall, and a few other items.')
                    print('Do you check your computer?')
                    while True:
                        r2 = input()
                        if r2 == 'Y':
                            clear()
                            comp_check = True
                            room()
                        elif r2 == 'N':
                            clear()
                            comp_check = False
                            room()
                        else:
                            print('Invalid response. Type capital "Y" or "N".')
                        room()
                elif r2 == 'N':
                    continue
                else:
                    print('Invalid response. Type capital "Y" or "N".')
        else:
            print('Invalid response. Type capital "Y" or "N".')

def room():
    global phone_check
    while True:
        print('\nLooking around, you can see you\'re into punk rock and anime. By the door, there\'s even an electric guitar and an amp.')
        if wallet_check == True:
            print('Now that you know your name, at least you have some kind of identity. Aiden Hendrix.')
        else:
            print('Since you didn\'t check your wallet, you\'re at a loss as to who you are.')
        if comp_check == True:
            print('You decide to find out some real information and get to the bottom of this. You sit down at your computer, which is unlocked, and open Google Chrome. You go to Facebook.com and see you\'re logged in as Aiden Hendrix.\nYou scroll through your timeline and do some investigation. You\'re an attractive young adult, although you look about fifteen. You\'re posing with a lot of filters and a few shirtless pictures, your body covered in tattoos.\nYou look at your friend\'s list, which is full of tons of attractive people, mostly female. You wonder if you are a model or a rockstar, or both.\nAfter a while, you decide to lie down and think things over.')
        if comp_check == False:
            print('You decided not to touch your computer and instead lie down on your bed, contemplating the situation.')
        if wallet_check == True or comp_check == True:
            print('Aiden Hendrix. You realize you\'re basically a kid with a decent apartment and everything you need in New York City. There is one bed, so you\'re living alone. You wonder where the income is from.')
        if wallet_check == False and comp_check == False:
            print('You lie on the bed, thinking, not even knowing your name. You realize you\'re basically a kid with a decent apartment and everything you need in New York City. There is one bed, so you\'re living alone. You wonder where the income is from.')
        print('\nSuddenly, your phone rings in your pocket. The ringtone is some kind of screamo song and it vibrates against your leg. The caller ID shows the name Vicky and a picture of an attractive girl.')
        while True:
            print('\nDo you answer the call?')
            r = input()
            if r == 'Y':
                clear()
                phone_check = True
                phone_call()
            elif r == 'N':
                clear()
                phone_check = False
                phone_call()
            else:
                print('Invalid response. Type capital "Y" or "N".')

def phone_call():
    global party_check
    global phone_check
    if phone_check == True or phone_check == 'missed':
        print('\nYou answer the phone and put it to your ear. You hear an enthusiastic female voice.')
        if phone_check == 'missed':
            print('\nVicky: Aiden! Dude wake up, you keep missing my calls.')
        elif phone_check == True:
            print('\nVicky: Aiden! Where are you? The party is starting soon!')
        if wallet_check == False and comp_check == False:
            print('\nNow you know your first name, at least.')
        else:
            print('\nYou wonder who this Vicky person is and what she wants.')
        print('\nYou: Hey... Vicky. I think I\'m dealing with some serious memory loss.. Maybe it\'s amnesia.')
        print('\nVicky: Wowza. Too many drugs? Well?? Are you coming or not? It\'s in room 104 in an hour.')
        print('\nDo you go to the party in room 104?')
        while True:
            r = input()
            if r == 'Y':
                clear()
                party_check = True
                print('\nYou: Yeah, I\'ll be there!')
                print('\nIt\'s worth checking out.')
                print('\nVicky: Okay, see you soon Aiden! I\'ll be right up.')
                highlife()
            elif r == 'N':
                clear()
                party_check = False
                print('\nYou: Erm.. No sorry, I\'ll have to take a rain check on this one.')
                print('\nVicky: Bummer, You\'re missing out. Gotta go, bye!')
                highlife()
            else:
                print('Invalid response. Type capital "Y" or "N".')
    elif phone_check == False:
        phone_check = True
        print('\nYou ignored the call. Five minutes later, it rings again.')
        print('Do you answer the call?')
        while True:
            r = input()
            if r == 'Y':
                clear()
                phone_call()
            elif r == 'N':
                clear()
                phone_check = 'missed'
                print('\nThe phone keeps ringing. This person is persistent.')
                print('Do you answer the call?')
            else:
                print('Invalid response. Type capital "Y" or "N".')

def highlife():
    global excited
    global mem_confess
    print('\nYou put your phone back into your pocket and rest for a bit.')
    print('After a few minutes, the door opens and Vicky walks in.')
    if party_check == True:
        print('\nVicky: AIDEN! Okay let\'s get you ready.')
        print('\nYou can\'t help but notice how incredibly sexy she is.')
        print('\nVicky: Are you excited or no?')
    elif party_check == False:
        print('\nVicky: Aiden, you have GOT to come to this get-together. Literally EVERYBODY that\'s somebody will be there.')
        print('\nYou stare at her, startled and in a daze, admiring how sexy she is.')
        print('\nVicky: Snap out of it! Let\'s get you ready. Are you excited or no?')

    done = True
    while done == True:
        r = input()
        if r == 'Y':
            clear()
            excited = True
            print('\nYou: Heck yeah I am!')
            done = False
        elif r == 'N':
            clear()
            excited = False
            print('\nYou: I mean... not exactly.')
            done = False
        else:
            print('Invalid response. Type capital "Y" or "N".')
    if excited == True:
        print('\nVicky: You better be, Aiden Hendrix!')
        if wallet_check == True or comp_check == True:
            print('\nYou smile back and stand up slowly.')
        elif wallet_check == False and comp_check == False:
            print('\nNow you know your last name: Hendrix. You stand up slowly.')
    elif excited == False:
        print('\nVicky: Bummer. Don\'t be loooooser...')
    print('\nYou follow her lead as she helps you undress and nearly shoves you in the shower.')
    print('\nVicky: Come on, you need to look AND smell good if we\'re gonna get spicy down there!')
    print('\nYou hop in the shower and, seconds later she joins you. To your surprise, she begins washing your hair and lathering soap over your body, making you wonder how high your status really is. She grabs a razor and begins going to work on your entire body.')
    print('\nVicky: Aiden boy you look confused. We do this every day. Want to tell me what you meant about memory loss?')
    print('\nVicky: Yay.. or nay?')
    while True:
        r = input()
        if r == 'Y':
            clear()
            mem_confess = True
            print('\nYou: I just don\'t remember anything. I didn\'t know my name, who I am, anything. Honestly.')
            print('\nVicky appears puzzled for a moment.')
            print('\nVicky: That\'s really scary Aiden. I\'ll do the best I can. I\'m here for you.')
            the_party()
        elif r == 'N':
            clear()
            mem_confess = False
            print('\nYou: I don\'t feel like talking about it right now. Maybe later...')
            print('\nVicky: Fine, but if you need me, I\'m here for you.')
            the_party()
        else:
            print('Invalid response. Type capital "Y" or "N".')

def the_party():
    global mem_escort
    print('\nAt this point, Vicky is drying you off and styling your long hair.')
    print('\nVicky: You\'re still my favorite emo kid, Aiden.')
    print('\nShe giggles, brushing your dyed black and red hair out of your face.')
    if comp_check == True:
        print('\nYou: I saw my Facebook. I sure post a lot... Who are all those people liking my stuff?')
        print('\nVicky: Ha! Facebook? You should see your Insta!')
    elif comp_check == False:
        print('\nYou: Who am I exactly? I never even checked my social media.')
        print('\nVicky: You\'re... very popular. And literally every slut wants you.')
    print('\nVicky: Anything else you wanna know, Aiden?')
    done = True
    while done == True:
        r = input()
        if r == 'Y':
            clear()
            mem_escort = True
            print('\nYou: Yes please.')
            print('\nVicky begins applying punk makeup around your eyes like a professional.')
            print('\nVicky: You\'re an artist, singer, guitarist, alternative model, an amazing friend, and... well... you\'re lowkey an escort.')
            print('\nThere is a short pause.')
            print('\nYou: Noted..')
            done = False
        elif r == 'N':
            clear()
            mem_escort = False
            print('\nYou: I think I\'ll take the blue pill for now.')
            print('\nVicky begins applying punk makeup around your eyes like a professional.')
            print('\nVicky: Suit yourself. You\'ll find out soon enough.')
            done = False
        else:
            print('Invalid response. Type capital "Y" or "N".')
    print('\nVicky: This will be the best cosplay party ever! Everybody will be a chick, even you!')
    print('\nNext thing you know, Vicky is pulling a pair of cute, laced panties over your junk and pulling a skirt out of a dresser.')
    if excited == True:
        print('\nVicky: You will be. So. Cute. Dude. Are you still excited?')
        print('\nShe sticks out her pierced tongue playfully at you.')
        room104()
    elif excited == False:
        print('\nVicky: You will be. So. Cute. Dude. Too bad you\'re not excited."')
        print('\nShe crosses her arms and pouts.')
        room104()

def room104():
    global complain
    global faggot
    done = True
    print('\nDo you complain about the situation?')
    while done == True:
        r = input()
        if r == 'Y':
            clear()
            complain = True
            print('\nYou: I don\'t know if I\'m so comfortable with this.')
            print('\nVicky: Stop. I will be here the whole time, right here next to you. You\'ll be fine, I promise.')
            done = False
        elif r == 'N':
            clear()
            complain = False
            print('\nYou: Alright, I\'m ready.')
            done = False
        else:
            print('Invalid response. Type capital "Y" or "N".')
    print('\nVicky grabs your wrists and slides a few stylish wristbands on you, before pulling you by your arm and leaving your apartment room. You follow her downstairs and you notice two cute school girls in the mirror, one pulling the other down the stairs.')
    print('\nVicky: Everyone at this cosplay party is dressed like a chick. You got a choice between a dog, or getting some of that cat.')
    print('\nVicky: So Aiden, are you going for cat? Ha! As if you can tell the difference! Until it\'s too late, that is.')
    print('\nYou feel yourself blush a little.')
    done = True
    print('\nDo you choose the female gender tonight?')
    while done == True:
        r = input()
        if r == 'Y':
            clear()
            faggot = False
            print('\nYou: I think it\'ll have to be real girls tonight.')
            print('\nVicky: Ayeee, Aiden! Get someeeeeee!!!')
            done = False
        elif r == 'N':
            clear()
            faggot = True
            print('\nYou: I\'m not gonna lie... I could use a good plowing...')
            print('\nVicky: AHHHH!!!!! YES!! Finally get some dick in that virgin boypussy of yours! God Aiden, that made me horny as FUCK. You\'re killing me!!')
            done = False
        else:
            print('Invalid response. Type capital "Y" or "N".')
    fucc()

def fucc():
    global kiss_vicky
    global faggot
    done = True
    while done == True:
        print('\nYou arrive at room 104. Before entering, do you kiss Vicky?')
        r = input()
        if r == 'Y':
            clear()
            kiss_vicky = True
            print('\nYou turn to Vicky and lean in for a kiss. She kisses you on the lips, beaming. You open the door and walk in.')
            done = False
        elif r =='N':
            clear()
            kiss_vicky = False
            print('\nYou open the door and walk in.')
            done = False
        else: print('Invalid response. Type capital "Y" or "N".')
    print('\nJack: AIDEN IS HERE!!!')
    if faggot == True:
        print('\nVicky: Aiden said he\'ll be the girl tonight, if you know what I mean!')
        print('\nThe sound of screaming girls and cheering boys penetrates your eyes, making you wince a little.')
    elif faggot == False and complain == False:
        print('\nVicky: Aiden the playa is here tonight. Girls, he\'s all yours.')
        print('\nYou watch as girls scream and cheer, and you dodge a few condoms. The males in the group look somewhat disappointed.')
    elif faggot == False and complain == True:
        print('\nVicky: Aiden the playa is here tonight. Girls, he\'s all yours.')
        print('\nYou watch as girls scream and cheer, and you dodge a few condoms. The males in the group look somewhat disappointed.')
        print('\nVicky: But wait... When I dressed Aiden up like a cute girl, he complained and said he wasn\'t comfortable. So he\'s reserved for you boys tonight.')
        print('\nA defeaning scream penetrates your eardrums as everyone basically stares at you in lust.')
        print('\nYou: V-Vicky... What the hell!?')
        print('\nShe just smiles and gives your ass a slap, before pushing you into the crowd of surprisingly attractive cross dressing dudes.')
    orgy()

def orgy():
    input("\nPress enter to continue...")
    clear()
    print('\nIt\'s on.')
    if faggot == True:
        gay()
    elif faggot == False and complain == False:
        straight()
    elif faggot == False and complain == True:
        bdsm()

def gay():
    global male_partner
    global female_partner
    print('\nYou walk over to the dudes, playing the part, showing off your completely smooth, hairless body. You drop jaws in your short skirt and hot school girl outfit, driving everyone crazy with your charisma. Jack, the hottest and coolest trap there, curtsies playfully. Others are looking jealous.')
    print('Do you choose Jack?')
    r = input()
    if r == 'Y':
        clear()
        print('\nYou nod in mutual agreement towards Jack, making eye contact with his mesmorizing eyes. You take his hand and he leads you into a bedroom.')
        print('\nJack: Fuck, you look so fucking hot Aiden. Want to top me, or...')
        done = True
        print('\nDo you top Jack?')
        while done == True:
            r = input()
            if r == 'Y':
                clear()
                print('\nYou nod, smiling. You pull off Jack\'s shirt slowly and begin kissing him. You unzip his skirt and let it naturally fall to the floor. With a gentle push, you bring him onto the bed, crawling on top of him, and sensually pull down his panties with your teeth. Still fully dressed, you climb on top, grinding against his cock and stroking his soft, hairless chest.')
                print('\nYou: God you are beyond sexy...')
                print('\nJack pushes you to your side and calmly reaches up your skirt, slowly sliding off your black laced panties. He motions you to climb back on top, which you do, gasping as you feel his rock hard cock gently glide against your tight, hairless boypussy.')
                print('\nJack: I feel like you like it a little rough, yeah?')
                done1 = True
                print('\nGo rough?')
                while done == True:
                    r = input()
                    if r == 'Y':
                        male_partner = 'jack_top_rough'
                        clear()
                        print('\nYou nod slightly, a smirk forming on your flawless face. You moan and arch your back as Jack slides his throbbing cock into your tight, virgin ass and starts fucking you like an animal. After two seconds you\'re hard as a rock, moaning and struggling in your slutty school girl costume, literally screaming like a girl. You feel Jack\'s hands reach under your skirt, grabbing and slapping your smooth ass hard as he goes balls deep in you.')
                        print('After about ten minutes of fucking, it all becomes too much and you feel your body tense up hard. You feel Jack\'s cock throbbing deep inside you, and with a mutual nod, you both close your eyes and hold on tight. A second later, you\'re screaming as intense pleasure runs through your body as you cum all over Jack\'s face. You cry out in pleasure as you feel his cock throb inside you, filling you up.')
                        print('\nYou: F-Fuckkk!!')
                        print('\nJack wraps his arms around your sensitive body and holds you close, snuggling you with his cock still far up your ass.')
                        print('\nJack: Holy fuck Aiden... You are so tight and so hot. That was fucking amazing. Better than any girl I fucked, by FAR.')
                        print('\nHe smiles up at you as your eyelids begin to close in exhaustion...')
                        input("\nPress enter to continue...")
                        clear()
                        after_sex()
                    elif r == 'N':
                        male_partner = 'jack_top_soft'
                        clear()
                        print('\nYou shake your head.')
                        print('\nYou: I\'m a soft boi I guess.')
                        print('\nJack: Okay, I hear you. I don\'t want to completely destroy that fine ass... Then again, I kinda do.')
                        print('\nHe grabs your waist, gently sliding your sensitive hole up and down against his cock, driving you crazy. Your soft high-pitched moans seem to turn him on as you feel his precum lube you up. You feel your face contort in pleasure as the sexual tension builds, helplessly resting your hands on his soft chest. A few minutes later, you let out a weak moan as Jack drives his throbbing cock into your tight boypussy.')
                        print('\nYou: Oh my god Jack... Please don\'t s-stop!')
                        print('\nThen as Jack goes deeper and deeper, slowly fucking your tight virgin ass, you feel him hit your male g-spot, making you cough and giggle as the front of your short skirt gets a very nice taste of your precum. The slow and steady sex is driving you fucking nuts, teasing your insides and causing you to tense up and twitch while your eyes roll back. With every twitch you tighten on Jack\'s throbbing cock, teasing your g-spot without mercy until your cute school girl skirt is fucking soaked.')
                        print('\nJack: Fuck you are tight, Aiden. Fuck heaven... I\'m here right now.')
                        print('\nAt least twenty minutes later, you\'re at your breaking point. Anything will make you cum right now, but Jack, sensing this, slows down, going extra deep.')
                        print('\nYou: Jaaaaaack...! I\'m about to lose it..')
                        print('\nAbout thirty seconds later, Jack lets out a sexy moan for a dude, filling you up like the good slut you are. You tense up hard against his pulsing cock, shaking and moaning weakly as all the stress leaves your body. A cute scream later, you cum your white, sticky love over Jack\'s chest and face, his cock stimulating your g-spot the entire time.')
                        print('You collapse into Jack\'s arms, opening your eyes just enough to notice his all-familiar look: That was hot and you know it.')
                        input("\nPress enter to continue...")
                        clear()
                        after_sex()
                    else:
                        invalid()
            elif r == 'N':
                male_partner = 'jack_bottom'
                clear()
                print('\nYou: That\'s your job, isn\'t it Jacky?')
                print('\nHe blushes for a moment, then picks you up like a feather and sets you on the bed, facing him. You both begin making out as he reaches up your skirt, slowly sliding off your panties so the lace glides along your smooth, shaved legs. You breathe deeply, anticipating what Jack will do next. Jack grabs your legs, checking them out for a few seconds before sliding off your thigh high socks. He starts kissing up your smooth legs, and you exhale softly.')
                print('\nYou: Nghhh... Jaaaack....')
                print('\nHe ignores your moans and continues making "love" to your legs, then calmly crawls up to your skirt and breathes in deeply, smelling you good.')
                print('\nJack: Mmmmmmm... God damn you smell so addictingly good. How the fuck are you such a yummy snack??')
                print('\nHe slides farther under your short skirt and you let out a cute, high-pitched moan as his tongue makes contact with your sweet boypussy. You continue moaning like an actual girl as he teases your tight asshole with his tongue. Every movement is unpredictable, driving you insane as you lay there, moaning helplessly. After ten minutes you\'re covered in your own sweet precum, which Jack has no problem cleaning off of you. Jack starts tongue-fucking you, driving his tongue into your ass while you moan louder in utter ecstasy.')
                print('Jack stops eating you out and grabs your throbbing cock with one hand, licking the tip as you feel two fingers slide right up against your male g-spot, fingering and massaging your biggest weak spot.')
                print('\nYou: J-Jaackk!!! F-F-Fuc-')
                print('\nBarley able to talk, you just moan helplessly as Jack takes your entire cock in his mouth.')
                input("\nPress enter to continue...")
                clear()
                print('\nJack stops sucking your throbbing, sensitive cock and takes his fingers out of your ass, licking them clean. He grabs your smooth legs and pins them down against your chest, holding you down. You feel completely exposed and vulnerable as Jack holds you like this, and without skipping a beat, he begins to rub his cock against your tight little boypussy. Jack drives it into you, almost painful. You moan like an animal in heat as your body naturally tenses with every throb of his cock. The pleasure of Jack\'s cock so deep inside you is so intense, you start hallucinating the golden gates.')
                print('After about fifteen more minutes of Jack\'s perfect slow pace, you both yell out in ungodly pleasure as Jack fills up another happy school girl. Jack drops next to you, pulling you into a deep, continuous kiss for at least half an hour.')
                input("\nPress enter to continue...")
                clear()
                after_sex()
            else:
                invalid()
    elif r == 'N':
        male_partner = 'kevin'
        clear()
        print('\nYou: I want to see what Kevin\'s got.')
        print('\nKevin: Big score!! I\'ve wanted to fuck that ass for years! You\'ll have a good ride.')
        print('\nJack looks confused, and a little betrayed. You follow Kevin, a more muscular trap, to the bedroom and he closes the door. He instantly strips off his costume and picks you up in his panties. He gets close to whisper in your ear.')
        print('\nKevin: I\'m going to fuck you senseless.')
        print('\nHe lifts you up higher and holds you against the nearest wall. You let out a soft grunt as he strips off your panties and drops his own. Kevin holds you up against the wall with his own strength, grabbing your thighs and kissing your neck aggressively. Oddly, the abuse and lack of control turns you on, but you\'re a bit nervous about this.')
        print('\nYou: W-Wait...')
        print('\nKevin ignores you and starts biting your neck, leaving hickies. You start to moan, giving into submission. In an effortless motion almost like lifting a child, he swings your legs up and atop his shoulders for a better position. Almost out of nowhere, Kevin backs up an inch as you drop onto his huge cock, which rams all the way up your tight, virgin ass. You scream as he fucks you against the wall, moaning and stretching your asscheeks so he can go deeper. You can\'t figure out if you\'re feeling more pleasure or pain at this point.')
        print('\nKevin: Stop squirming and take it you slut!')
        print('\nYou try to scream and get away, but he stuffs your own panties in your mouth and holds your wrists to your sides. No matter how hard you struggle, you can\'t get away or scream loud enough for anyone to hear. You get so worked up in your struggle, you don\'t notice how hard and wet you are until it\'s running down your cock and down your smooth, hairless balls. About ten minutes in, you\'re already about to cum. Your body is twitching and sensitive all over.')
        print('\nYou: Mmmhmmmhmmmmmmm....!')
        print('\nHe laughs and moans as he fucks every last ounce of your spirit and energy out of you. With a loud animalistic moan, Kevin rams balls deep into you, forcing you to cum your heart out as he finishes in your tight, innocent ass. You fall to the floor, shaking and sobbing into your panties until you pass out.')
        input("\nPress enter to continue...")
        clear()
        after_sex()
    else:
        invalid()


                    


def straight():
    global female_partner
    global male_partner
    female_partner = True
    male_partner = False
    print('\nYou walk towards the group of girls, making sure none of them are traps in disguise. One thing is for sure, they are all HOT.')
    if excited == False:
        print('\nVicky: And Aiden thought it wouldn\'t be exciting!')
    print('\nAlyssa: Hey Aiden, follow us into the girls\' room. I\'m gonna suck your cock all night until you cum, while Britney sits on your face. Then we\'ll take turns fucking. Sounds good?')
    print('\nYou can\'t stop smirking at the situation. What a memory to wake up into. You nod in approval and follow the cosplayers into the bedroom. It smells amazing and you take a seat on the bed, being the only "male" school girl in the room.')
    print('\nBritney: Gosh, you have such a cute face, Aiden. Handsome boi. I need to sit on that.')
    print('\nYou can feel the heat creeping up on your face as you blush, hard. The girl named Alyssa pushes you onto your back and starts planting wet kisses all over your face. The girls undress your top and remove your socks, keeping only your short skirt and panties on.')
    print('\nAlyssa: Your punk makeup is so hardcore!')
    print('\nBritney: I think it\'s fucking adorable.')
    print('\nYou can already tell Alyssa is the leader of the pack, even though she is the smallest. Alyssa tosses off her clothes, revealing white and pink lingerie. She mounts you, running her hands down your bare chest as she grinds softly. You feel a bulge in your tight panties, just as Britney takes a seat directly on your face. She grabs your wrists, guiding them to Alyssa\'s tits as Britney\'s sexy, fit ass gets comfortable on your face.')
    print('\nBritney: You\'re even cuter when you\'re submissive, Aiden.')
    print('\nYou couldn\'t blush harder if you were on live TV right now. Britney\'s ass tastes amazing as you lick her clit over and over, eating out her perfect ass. Alyssa stops grinding on your now raging cock and begins pulling your laced panties down, leaving them just above your knees.')
    print('\nAlyssa: Aww, these are so cute Aiden! They\'re perfect for you!')
    input("\nPress enter to continue...")
    clear()
    print('\nYou move your hands to grab Britney\'s ass, spreading it to properly eat her out. As your tongue explores her clit more and more, she arches her back moaning, pressing into your face. She\'s so wet you can barely keep up at this point. You lick up every drop of her hot mess, savoring each moment while it runs down your throat.')
    print('Meanwhile, Alyssa has found your dick and her tongue is running circles around the tip. You moan weakly, drowning in Britney\'s hot pussy mess while Alyssa starts deepthroating you slowly. At this point, you really can\'t keep up. Your face is soaked trying to eat out Britney, and Alyssa is just double teaming by sucking your cock. You suddenly feel Alyssa sneaking a couple fingers deep up your butt. You feel her nails slide up and trigger your weak spot, pressing on it and stroking directly on your g-spot with her nails.')
    print('\nYou: A-aahhh!!! A-alyssa.......')
    print('\nAlyssa: Mmm, that\'s a lot of precum Aiden. You taste so fucking good.')
    print('\nThe butt stimulation plus deepthroating is driving you fucking crazy. You moan loudly like a little slut as you eat Britney out. She strokes your chest soothingly, moaning in obvious pleasure. Alyssa keeps sucking you slow, then as her nails drag across your g-spot just a little more aggressively, you scream in intense pleasure and orgasm hard into Alyssa\'s mouth in a wave of pleasure.')
    print('You lay there, completely exhausted. You didn\'t even notice Britney cum on your face. The girls get close to you, cuddling and stroking your body as Alyssa pulls your panties back on.')
    print('\nBritney: Woah Aiden, you are such a fucking stud. So hot!')
    print('\nAlyssa: You\'re actually amazing, Aiden. And your makeup is still perfect! We\'ll clean you up and you can sleep with us. You deserve it.')
    input("\nPress enter to continue...")
    clear()
    after_sex()














def bdsm():
    print('\nThis is the BDSM scene.')





def after_sex():
    if male_partner == 'jack_top_rough':
        print('You topped Jack and it was rough.')
    elif male_partner == 'jack_top_soft':
        print('You topped Jack and it was gentle.')
    elif male_partner == 'jack_bottom':
        print('You had hot bottom sex with Jack.')
    elif male_partner == 'kevin':
        print('You were raped by Kevin.')
    elif female_partner == True:
        print('You traded oral sex with Alyssa and Britney.')
    elif male_partner == 'bdsm':
        print('')








def begin():
    print('\nYou are standing outside a large apartment building in New York. You have lost most of your memories and your past is all a blur. You look down at yourself and see you are wearing black skinny jeans and custom Vans skate shoes.\nDo you enter the apartment?')
    story()



print('Welcome. To begin, type "start." Type "exit" to leave at any time.')
print('Notice: All future responses must be Y or N.')
while True:
    r = input()
    if r == 'exit':
        print('Exiting...')
        exit()
    elif r == 'start':
        clear()
        begin()
    else:
        print('Invalid response. Type "start" or "exit".')
